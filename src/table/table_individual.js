/// import React, { useState } from 'react';
import React, { useState } from 'react';

import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';

// function createData(name, calories, fat, carbs, protein) {
//   return { name, calories, fat, carbs, protein };
// }

function createData(month, chama_date, chama_house, savings, loans_borrowed) {
    return { month, chama_date, chama_house, savings,loans_borrowed };
  }




const rows = [
//   createData('January', 1/8/2023, 'Julia', 2000, 2750 ),
  createData('JAN', '1/8/2023', 'JULIA', 2000, 2750),
  createData('FEB', '1/8/2023', 'JENNIFER', 2000, 10000),
  createData('MAR', '1/8/2023', 'CARO 1', 2000, 5000),
  createData('APR', '1/8/2023', 'ROSE', 2000, 6000),
  createData('MAY', '1/8/2023', 'JUDY', 2000, 4000),
  createData('JUN', '1/8/2023', 'HELDA', 2000, 2000),
  createData('JUL', '1/8/2023', 'CARO B', 2000, 3000),
  createData('AUG', '1/8/2023', 'CARO B', 2000, 3000),
  createData('SEP', '1/8/2023', 'CARO B', 2000, 3000),
  createData('OCT', '1/8/2023', 'CARO B', 2000, 3000),
  createData('NOV', '1/8/2023', 'CARO B', 2000, 3000),
  createData('DEC', '1/8/2023', 'CARO B', 2000, 3000),




];





export default function BasicTable() {

  const [hoveredRow, setHoveredRow] = useState(null);

  function handleRowClick (row){
    console.log('Clicked row:', row);
  };

  const handleRowHover = (event, row) => {
    setHoveredRow(row);
  };

  const handleRowHoverEnd = () => {
    setHoveredRow(null);
  };


  return (

    <div>
    <h1 className='member'>Julia</h1>  


    <TableContainer component={Paper}>
      <Table sx={{ minWidth: 650 }} aria-label="simple table">
     
        <TableHead>
          <TableRow>
            <TableCell>month</TableCell>
            <TableCell align="right">chama_date</TableCell>
            <TableCell align="right">chama_house&nbsp;</TableCell>
            <TableCell align="right">Savings&nbsp;(KES)</TableCell>
            <TableCell align="right">Loans_borrowed&nbsp;(KES)</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <TableRow
              key={row.name}
              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
              onClick={handleRowClick(row.chama_date)}

             
              onMouseEnter={(event) => handleRowHover(event, row)}
              onMouseLeave={handleRowHoverEnd}
              style={row === hoveredRow ? { backgroundColor: 'lightgray' } : {}}

            >


            <TableCell component="th" scope="row"> {row.month}</TableCell>
              <TableCell align="right">{row.chama_date}</TableCell>
              <TableCell align="right">{row.chama_house}</TableCell>
              <TableCell align="right">{row.savings}</TableCell>
              <TableCell align="right">{row.loans_borrowed}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
        
    </div>
  );
}
