import { useState } from "react";
import './profile.css'



const Card = () => {
    const [name_, setName] = useState('your name')
    const [job, setJob] = useState('your name')
    const [about, setAbout] = useState('sdsdsdsdddddddddddddsdsdsdddddddddddddddddddsdsdsdsdsdsdsdsdsdsdsdsdsdse')

    return ( 

        <div className='Card'>
            <div className="upper-container">
                <div className="image-container">
                    <img src="https://www.google.com/search?q=mark+zuckerber+profile+picture&oq=mark+zuckerber+profile+picture&aqs=chrome..69i57j0i13i512j0i22i30l2j0i390i650l5.6767j0j7&sourceid=chrome&ie=UTF-8#imgrc=NqYz7f1b7aMUsM" alt="" height="100px" width="100px"/>
                </div>
            </div>

            <div className="lower-container">
                <h3>{name_}</h3>
                <h4>{ job }</h4>
                <p>{ about }</p>
                <button>Visit profile</button>
            </div>

        </div>
     );
}
 
export default Card;