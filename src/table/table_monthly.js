import MUIDataTable from 'mui-datatables';
// import { data } from '../data';
export const data = [
  {
    id: 1,
    title: 'The Hunger Games',
    authors: 'Suzanne Collins',
    num_pages: 374,
    rating: 4.33
  },
  {
    id: 2,
    title: 'Harry Potter and the Order of the Phoenix',
    authors: 'J.K. Rowling',
    num_pages: 870,
    rating: 4.48
  },
  {
    id: 3,
    title: 'To Kill a Mockingbird',
    authors: 'Harper Lee',
    num_pages: 324,
    rating: 4.27
  },
  {
    id: 4,
    title: 'Pride and Prejudice',
    authors: 'Jane Austen',
    num_pages: 279,
    rating: 4.25
  },
  {
    id: 5,
    title: 'Twilight',
    authors: 'Stephenie Meyer',
    num_pages: 498,
    rating: 3.58
  },
  {
    id: 6,
    title: 'The Book Thief',
    authors: 'Markus Zusak',
    num_pages: 552,
    rating: 4.36
  }
];


export const data_1 = [
  {
    id: 1,
    month: 'JAN',
    chama_date: '1/8/2023',
    chama_house: 'Julia',
    Savings: 2000,
    Loans_borrowed: 2750,
  },
  {
    id: 2,
    month: 'FEB',
    chama_date: '1/8/2023',
    chama_house: 'Jennifer',
    Savings: 2000,
    Loans_borrowed: 2750,
  },
  {
    id: 3,
    month: 'MAR',
    chama_date: '1/8/2023',
    chama_house: 'Caro 1',
    Savings: 2000,
    Loans_borrowed: 2750,
  },
  {
    id: 4,
    month: 'APR',
    chama_date: '1/8/2023',
    chama_house: 'Rose',
    Savings: 2000,
    Loans_borrowed: 2750,
  },
  {
    id: 5,
    month: 'MAY',
    chama_date: '1/8/2023',
    chama_house: 'Judy',
    Savings: 2000,
    Loans_borrowed: 2750,
  },
  {
    id: 6,
    month: 'JUN',
    chama_date: '1/8/2023',
    chama_house: 'Helda',
    Savings: 2000,
    Loans_borrowed: 2750,
  }
];



const MUITable = () => {
  const columns = [
    { label: 'Month', name: 'month' },
    { label: 'Chama_date', name: 'chama_date' },
    { label: 'Chama_house', name: 'chama_house', options: { sort: true } },
    { label: 'Savings (KES)', name: 'Savings' },
    { label: 'Loans_borrowed (KES)', name: 'Loans_borrowed' }
  ];
  const options = {
    filterType: 'checkbox',
    onRowClick: (rowData, rowMeta) => {
      console.log('Clicked on row:', rowData, rowMeta);

      return (
        <h1>Row clicked</h1>
      )
    }
  };
  return (
    <div style={{ maxWidth: '200%' }}>
      <MUIDataTable
        columns={columns}
        data={data_1}
        title='Julia'
        options={options}
      />
    </div>
  );
};
export default MUITable;