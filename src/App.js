import Navbar from './Navbar';
import Home from './Home';
import Create_form from './form/form';
import Card from './card/profile';
import BasicTable from './table/table_individual';
import MUITable from './table/table_monthly'

function App() {
  const title = "Welcome to the new Blog"
  return (
    <div className="App">
      <Navbar/>
      <div className="content">
      {/* <Home/>  */}
      <MUITable/>
        {/* <Home/> */}
        {/* <Create_form/> */}
        {/* <BasicTable/> */}
      </div>
    </div>
  );
}

export default App;
